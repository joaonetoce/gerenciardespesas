package gerenciador.util;

import gerenciador.despesa.DespesaDAO;
import gerenciador.despesa.DespesaDAOHibernate;
import gerenciador.despesavariavel.DespesaVariavelDAO;
import gerenciador.despesavariavel.DespesaVariavelDAOHibernate;
import gerenciador.familia.FamiliaDAO;
import gerenciador.familia.FamiliaDAOHibernate;
import gerenciador.financeiro.FinanceiroDAO;
import gerenciador.financeiro.FinanceiroDAOHibernate;
import gerenciador.rendaadicional.RendaAdicionalDAO;
import gerenciador.rendaadicional.RendaAdicionalDAOHibernate;
import gerenciador.teste.TesteDAO;
import gerenciador.teste.TesteDAOHibernate;
import gerenciador.usuario.UsuarioDAO;
import gerenciador.usuario.UsuarioDAOHibernate;

public class DAOFactory {

	public static UsuarioDAO criarUsuarioDAO(){
		UsuarioDAOHibernate usuarioDAO = new UsuarioDAOHibernate();
		usuarioDAO.setSession(HibernateUtil.getSessionFactory().getCurrentSession());
		return usuarioDAO;
	}
	
	public static FamiliaDAO criarFamiliaDAO(){
		FamiliaDAOHibernate familiaDAO = new FamiliaDAOHibernate();
		familiaDAO.setSession(HibernateUtil.getSessionFactory().getCurrentSession());
		return familiaDAO;
	}
	
	public static DespesaDAO criarDespesaDAO(){
		DespesaDAOHibernate despesaDAO = new DespesaDAOHibernate();
		despesaDAO.setSession(HibernateUtil.getSessionFactory().getCurrentSession());
		return despesaDAO;
	}
	
	public static FinanceiroDAO criarFinanceiroDAO(){
		FinanceiroDAOHibernate financeiroDAO = new FinanceiroDAOHibernate();
		financeiroDAO.setSession(HibernateUtil.getSessionFactory().getCurrentSession());
		return financeiroDAO;
	}
	
	public static RendaAdicionalDAO criarRendaAdicionalDAO(){
		RendaAdicionalDAOHibernate rendaAdicionalDAO = new RendaAdicionalDAOHibernate();
		rendaAdicionalDAO.setSession(HibernateUtil.getSessionFactory().getCurrentSession());
		return rendaAdicionalDAO;
	}
	
	public static DespesaVariavelDAO criarDespesaVariavelDAO(){
		DespesaVariavelDAOHibernate despesaVariavelDAO = new DespesaVariavelDAOHibernate();
		despesaVariavelDAO.setSession(HibernateUtil.getSessionFactory().getCurrentSession());
		return despesaVariavelDAO;
	}
	
	public static TesteDAO criarTesteDAO(){
		TesteDAOHibernate testeDAO = new TesteDAOHibernate();
		testeDAO.setSession(HibernateUtil.getSessionFactory().getCurrentSession());
		return testeDAO;
	}
}
