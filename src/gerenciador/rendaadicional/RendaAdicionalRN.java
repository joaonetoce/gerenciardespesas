package gerenciador.rendaadicional;

import java.util.Calendar;
import java.util.List;

import gerenciador.familia.Familia;
import gerenciador.familia.FamiliaRN;
import gerenciador.usuario.Usuario;
import gerenciador.util.DAOFactory;
import gerenciador.util.MyLibrary;

public class RendaAdicionalRN {

	private RendaAdicionalDAO rendaAdicionalDAO;
	
	public RendaAdicionalRN() {
		this.rendaAdicionalDAO = DAOFactory.criarRendaAdicionalDAO();
	}
	
	public RendaAdicional carregar(Integer codigo){
		return this.rendaAdicionalDAO.carregar(codigo);
	}
	
	public void salvar(RendaAdicional rendaAdicional){
			
		MyLibrary library = new MyLibrary();
		Usuario usuarioLogado = library.buscarUsuarioLogado();
		
		FamiliaRN familiaRN = new FamiliaRN();
		Familia familia = familiaRN.carregar(usuarioLogado.getFamilia().getCod_familia());
		familia.setAtualizacao_renda(Calendar.getInstance());
		
		rendaAdicional.setData_atualizacao(Calendar.getInstance());
		
		Integer codigo = rendaAdicional.getCod_rendaadicional();
		if(codigo == null || codigo == 0){
						
			rendaAdicional.setFamilia(familia);
			rendaAdicional.setData_cadastro(Calendar.getInstance());
			this.rendaAdicionalDAO.salvar(rendaAdicional);
			
			float rendaAdicionalFinal = familia.getRenda_adicional() + rendaAdicional.getValor();
			familia.setRenda_adicional(rendaAdicionalFinal);
			this.rendaAdicionalDAO.atualizarFamilia(familia);
		}else{
			
			RendaAdicional rendaAdicionalAntiga = this.carregar(rendaAdicional.getCod_rendaadicional());
			float rendaAdicionalTotal = familia.getRenda_adicional();
			float valorRendaAdicionalAntiga = rendaAdicionalAntiga.getValor();
			float novoValorRendaAdicional = rendaAdicional.getValor();
			float rendaAdicionalFinal = (rendaAdicionalTotal - valorRendaAdicionalAntiga) + novoValorRendaAdicional;
			
			this.rendaAdicionalDAO.atualizar(rendaAdicional);
			
			familia.setRenda_adicional(rendaAdicionalFinal);			
			this.rendaAdicionalDAO.atualizarFamilia(familia);			
		}
	}
	
	public void excluir(RendaAdicional rendaAdicional){
		MyLibrary library = new MyLibrary();		
		Usuario usuarioLogado = library.buscarUsuarioLogado();
		if(usuarioLogado.getTipo() == 1){
			FamiliaRN familiaRN = new FamiliaRN();
			Familia familia = familiaRN.carregar(usuarioLogado.getFamilia().getCod_familia());
			familia.setAtualizacao_renda(Calendar.getInstance());
		
			float rendaAdicionalFinal = familia.getRenda_adicional() - rendaAdicional.getValor();
			familia.setRenda_adicional(rendaAdicionalFinal);
			this.rendaAdicionalDAO.atualizarFamilia(familia);
		}
		this.rendaAdicionalDAO.excluir(rendaAdicional);
	}
	
	public List<RendaAdicional> listar(){
		MyLibrary library = new MyLibrary();
		Usuario usuarioLogado = library.buscarUsuarioLogado();
		return this.rendaAdicionalDAO.listar(usuarioLogado.getFamilia());
	}
}
