package gerenciador.rendaadicional;

import gerenciador.familia.Familia;

import java.util.List;

public interface RendaAdicionalDAO {

	public void salvar(RendaAdicional rendaAdicional);
	public void atualizar(RendaAdicional rendaAdicional);
	public void atualizarFamilia(Familia familia);
	public void excluir(RendaAdicional rendaAdicional);
	public RendaAdicional carregar(Integer codigo);
	public List<RendaAdicional> listar(Familia familia);
}
