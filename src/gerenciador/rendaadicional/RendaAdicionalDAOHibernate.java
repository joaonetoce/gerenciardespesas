package gerenciador.rendaadicional;

import gerenciador.familia.Familia;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class RendaAdicionalDAOHibernate implements RendaAdicionalDAO {

	private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}
	
	@Override
	public void salvar(RendaAdicional rendaAdicional) {
		this.session.save(rendaAdicional);		
	}

	@Override
	public void atualizar(RendaAdicional rendaAdicional) {
		String hql = "update RendaAdicional set valor = :valor, descricao = :descricao, data_atualizacao = :data_atualizacao where cod_rendaadicional = :cod_rendaadicional";
		Query update = session.createQuery(hql);
		update.setParameter("cod_rendaadicional", rendaAdicional.getCod_rendaadicional());
		update.setParameter("data_atualizacao", rendaAdicional.getData_atualizacao());
		update.setParameter("valor", rendaAdicional.getValor());
		update.setParameter("descricao", rendaAdicional.getDescricao());
		update.executeUpdate();
	}

	@Override
	public void excluir(RendaAdicional rendaAdicional) {
		this.session.delete(rendaAdicional);		
	}

	@Override
	public RendaAdicional carregar(Integer codigo) {
		return (RendaAdicional) this.session.get(RendaAdicional.class, codigo);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RendaAdicional> listar(Familia familia) {
		Criteria crit = session.createCriteria(RendaAdicional.class);
		crit.add(Restrictions.eq("familia",familia));		
		return crit.list();
	}

	@Override
	public void atualizarFamilia(Familia familia) {
		String hql = "update Familia set atualizacao_renda = :atualizacao_renda, renda_adicional = :renda_adicional where cod_familia = :cod_familia";
		Query update = session.createQuery(hql);
		update.setParameter("cod_familia", familia.getCod_familia());
		update.setParameter("atualizacao_renda", familia.getAtualizacao_renda());
		update.setParameter("renda_adicional", familia.getRenda_adicional());
		update.executeUpdate();
		
		System.out.println("rendaAdicionalFinal: " + familia.getRenda_adicional());
	}

}
