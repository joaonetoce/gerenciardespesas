package gerenciador.usuario;

import gerenciador.familia.Familia;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class UsuarioDAOHibernate implements UsuarioDAO {

	private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}
	
	@Override
	public void salvar(Usuario usuario) {
		this.session.save(usuario);
	}

	@Override
	public void atualizar(Usuario usuario) {
		if (usuario.getPermissao() == null || usuario.getPermissao().size() == 0) {
			Usuario usuarioPermissao = this.carregar(usuario.getCod_usuario());
			usuario.setPermissao(usuarioPermissao.getPermissao());
			this.session.evict(usuarioPermissao);
		}
		//this.session.update(usuario);
		String hql = "update Usuario set orcamento = :orcamento, cep = :cep, complemento = :complemento, bairro = :bairro, numero = :numero, endereco = :endereco, datanascimento = :datanascimento, telefone = :telefone, cpf = :cpf, sobrenome = :sobrenome, atualizacao_cadastro = :atualizacao_cadastro, nome = :nome, tipo = :tipo, email = :email, ativo = :ativo, cod_familia = :cod_familia where cod_usuario = :cod_usuario";
		Query update = session.createQuery(hql);
		update.setParameter("cod_usuario", usuario.getCod_usuario());
		update.setParameter("atualizacao_cadastro", usuario.getAtualizacao_cadastro());
		update.setParameter("nome", usuario.getNome());
		update.setParameter("email", usuario.getEmail());
		update.setParameter("ativo", usuario.isAtivo());
		update.setParameter("tipo", usuario.getTipo());
		update.setParameter("cod_familia", usuario.getFamilia().getCod_familia());
		update.setParameter("orcamento", usuario.getOrcamento());
		update.setParameter("sobrenome", usuario.getSobrenome());
		update.setParameter("cpf", usuario.getCpf());
		update.setParameter("telefone", usuario.getTelefone());
		update.setParameter("datanascimento", usuario.getDatanascimento());
		update.setParameter("endereco", usuario.getEndereco());
		update.setParameter("numero", usuario.getNumero());
		update.setParameter("bairro", usuario.getBairro());
		update.setParameter("complemento", usuario.getComplemento());
		update.setParameter("cep", usuario.getCep());
		//int result = update.executeUpdate();
		update.executeUpdate();	
	}

	@Override
	public void excluir(Usuario usuario) {
		this.session.delete(usuario);
	}

	@Override
	public Usuario carregar(Integer codigo) {
		return (Usuario) this.session.get(Usuario.class, codigo);
	}

	@Override
	public Usuario buscarPorLogin(String login) {
		String hql = "select u from Usuario u where u.login = :login";
		Query consulta = this.session.createQuery(hql);
		consulta.setString("login", login);
		return (Usuario)consulta.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> listar(Integer cod_usuario, Integer tipo, Integer tipoUserLogado, Familia familia) {
		//return this.session.createCriteria(Usuario.class).list();
		if(tipoUserLogado == 1){
			Criteria crit = session.createCriteria(Usuario.class);
			crit.add(Restrictions.eq("tipo",tipo));
			crit.add(Restrictions.eq("familia",familia));
			crit.add(Restrictions.ne("cod_usuario",cod_usuario));
			crit.addOrder(Order.desc("familia"));
			return crit.list();
		}else{
			Criteria crit = session.createCriteria(Usuario.class);
			crit.add(Restrictions.eq("tipo",tipo));
			crit.add(Restrictions.ne("cod_usuario",cod_usuario));
			crit.addOrder(Order.desc("familia"));
			return crit.list();
		}
	}

	@Override
	public void atualizarAdministrador(Usuario usuario) {
		if (usuario.getPermissao() == null || usuario.getPermissao().size() == 0) {
			Usuario usuarioPermissao = this.carregar(usuario.getCod_usuario());
			usuario.setPermissao(usuarioPermissao.getPermissao());
			this.session.evict(usuarioPermissao);
		}
		//this.session.update(usuario);
		String hql = "update Usuario set cep = :cep, complemento = :complemento, bairro = :bairro, numero = :numero, endereco = :endereco, datanascimento = :datanascimento, telefone = :telefone, cpf = :cpf, sobrenome = :sobrenome, atualizacao_cadastro = :atualizacao_cadastro, nome = :nome, tipo = :tipo, email = :email, ativo = :ativo where cod_usuario = :cod_usuario";
		Query update = session.createQuery(hql);
		update.setParameter("cod_usuario", usuario.getCod_usuario());
		update.setParameter("atualizacao_cadastro", usuario.getAtualizacao_cadastro());
		update.setParameter("nome", usuario.getNome());
		update.setParameter("email", usuario.getEmail());
		update.setParameter("ativo", usuario.isAtivo());
		update.setParameter("tipo", usuario.getTipo());
		
		update.setParameter("sobrenome", usuario.getSobrenome());
		update.setParameter("cpf", usuario.getCpf());
		update.setParameter("telefone", usuario.getTelefone());
		update.setParameter("datanascimento", usuario.getDatanascimento());
		update.setParameter("endereco", usuario.getEndereco());
		update.setParameter("numero", usuario.getNumero());
		update.setParameter("bairro", usuario.getBairro());
		update.setParameter("complemento", usuario.getComplemento());
		update.setParameter("cep", usuario.getCep());
		//int result = update.executeUpdate();
		update.executeUpdate();		
	}

	@Override
	public void alterarSenha(Usuario usuario) {
		String hql = "update Usuario set senha = :senha where cod_usuario = :cod_usuario";
		Query update = session.createQuery(hql);
		update.setParameter("cod_usuario", usuario.getCod_usuario());
		update.setParameter("senha", usuario.getSenha());
		//int result = update.executeUpdate();
		update.executeUpdate();		
	}	
}