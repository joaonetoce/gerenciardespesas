package gerenciador.usuario;

import gerenciador.familia.Familia;

import java.util.List;

public interface UsuarioDAO {

	public void salvar(Usuario usuario);
	public void atualizar(Usuario usuario);
	public void atualizarAdministrador(Usuario usuario);
	public void alterarSenha(Usuario usuario);
	public void excluir(Usuario usuario);
	public Usuario carregar(Integer codigo);
	public Usuario buscarPorLogin(String login);
	public List<Usuario> listar(Integer cod_usuario, Integer tipo, Integer tipoUserLogado, Familia familia);
}
