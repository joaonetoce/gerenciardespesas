package gerenciador.usuario;

import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;

import org.springframework.security.core.context.SecurityContextHolder;

import gerenciador.util.DAOFactory;
import gerenciador.util.MyLibrary;

public class UsuarioRN {

	private UsuarioDAO usuarioDAO;

	public UsuarioRN() {
		this.usuarioDAO = DAOFactory.criarUsuarioDAO();
	}
	
	public Usuario carregar(Integer codigo){
		return this.usuarioDAO.carregar(codigo);
	}
	
	public Usuario buscarPorLogin(String login){
		return this.usuarioDAO.buscarPorLogin(login);
	}
	
	public void salvar(Usuario usuario) throws NoSuchAlgorithmException{
		Integer codigo = usuario.getCod_usuario();
		MyLibrary library = new MyLibrary();
		usuario.setAtualizacao_cadastro(Calendar.getInstance());
		if(usuario.getTipo() == 0){ //Se o usuário é um administrador
			if(codigo == null || codigo == 0){
				usuario.setOrcamento(0);
	            String senhaCrp = library.encriptarSenha("1234");
				usuario.setSenha(senhaCrp);
				usuario.setLogin(usuario.getEmail());
				usuario.setData_cadastro(Calendar.getInstance());
				usuario.setAtivo(true);			
				usuario.getPermissao().add("ROLE_ADMINISTRADOR");
				usuarioDAO.salvar(usuario);			
			}else{			
				this.usuarioDAO.atualizarAdministrador(usuario);			
			}
		}else{
			if(codigo == null || codigo == 0){
				usuario.setOrcamento(0);
	            String senhaCrp = library.encriptarSenha("1234");
				usuario.setSenha(senhaCrp);
				usuario.setLogin(usuario.getEmail());
				usuario.setData_cadastro(Calendar.getInstance());
				usuario.setAtivo(true);			
				usuario.getPermissao().add("ROLE_USUARIO");								
				usuarioDAO.salvar(usuario);			
			}else{			
				this.usuarioDAO.atualizar(usuario);			
			}
		}	
	}
	
	public void alterarSenha(Usuario usuario) throws NoSuchAlgorithmException{
		MyLibrary library = new MyLibrary();
		String senhaCriptografada = library.encriptarSenha(usuario.getSenha());
		usuario.setSenha(senhaCriptografada);
		this.usuarioDAO.alterarSenha(usuario);
	}
	
	public void excluir(Usuario usuario){
		this.usuarioDAO.excluir(usuario);
	}
	
	public List<Usuario> listarUsuarios(){
		String loginUserLog = SecurityContextHolder.getContext().getAuthentication().getName();
		Usuario usuarioLogado = this.buscarPorLogin(loginUserLog);		
		return this.usuarioDAO.listar(usuarioLogado.getCod_usuario(), 1, usuarioLogado.getTipo(), usuarioLogado.getFamilia());
	}
	
	public List<Usuario> listarAdministradores(){
		String loginUserLog = SecurityContextHolder.getContext().getAuthentication().getName();
		Usuario usuarioLogado = this.buscarPorLogin(loginUserLog);		
		return this.usuarioDAO.listar(usuarioLogado.getCod_usuario(), 0, usuarioLogado.getTipo(), usuarioLogado.getFamilia());
	}	
}