package gerenciador.despesa;

import java.util.Calendar;
import java.util.List;

import gerenciador.familia.Familia;
import gerenciador.familia.FamiliaRN;
import gerenciador.usuario.Usuario;
import gerenciador.util.DAOFactory;
import gerenciador.util.MyLibrary;

public class DespesaRN {

	private DespesaDAO despesaDAO;

	public DespesaRN() {
		this.despesaDAO = DAOFactory.criarDespesaDAO();
	}
	
	public Despesa carregar(Integer codigo){
		return this.despesaDAO.carregar(codigo);
	}
		
	public void salvar(Despesa despesa){
		
		MyLibrary library = new MyLibrary();		
		Usuario usuarioLogado = library.buscarUsuarioLogado();		
		FamiliaRN familiaRN = new FamiliaRN();
		Familia familia = familiaRN.carregar(usuarioLogado.getFamilia().getCod_familia());
		familia.setAtualizacao_renda(Calendar.getInstance());	
		despesa.setData_atualizacao(Calendar.getInstance());
		Integer codigo = despesa.getCod_despesa();
		if(codigo == null || codigo == 0){
			despesa.setData_cadastro(Calendar.getInstance());
			despesa.setFamilia(familia);
			
			/*----------------------------------------------------------*/
						
			float debitoFinal = familia.getDebito() + despesa.getValor();
			familia.setDebito(debitoFinal);
			
			this.despesaDAO.atualizarFamilia(despesa, familia);
			
			/*----------------------------------------------------------*/
			
			this.despesaDAO.salvar(despesa);
			
		}else{	
			
			/*----------------------------------------------------------*/
			
			Despesa despesaAntiga = carregar(despesa.getCod_despesa());						
			float debitoFinal = (familia.getDebito() - despesaAntiga.getValor()) + despesa.getValor();
			familia.setDebito(debitoFinal);
					
			this.despesaDAO.atualizarFamilia(despesa, familia);
			
			/*----------------------------------------------------------*/
			
			this.despesaDAO.atualizar(despesa);
			
		}
	}
	
	public void excluir(Despesa despesa){
		
		MyLibrary library = new MyLibrary();		
		Usuario usuarioLogado = library.buscarUsuarioLogado();
		
		if(usuarioLogado.getTipo() == 1){
			FamiliaRN familiaRN = new FamiliaRN();
			Familia familia = familiaRN.carregar(usuarioLogado.getFamilia().getCod_familia());
			familia.setAtualizacao_renda(Calendar.getInstance());
		
			float debitoFinal = familia.getDebito() - despesa.getValor();
			familia.setDebito(debitoFinal);
			this.despesaDAO.atualizarFamilia(despesa, familia);	
		}
		this.despesaDAO.excluir(despesa);
	}
	
	public List<Despesa> listar(){
		MyLibrary library = new MyLibrary();
		Usuario usuarioLogado = library.buscarUsuarioLogado();
		return this.despesaDAO.listar(usuarioLogado.getFamilia());
	}
}