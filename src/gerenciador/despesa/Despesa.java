package gerenciador.despesa;

import gerenciador.familia.Familia;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="despesa")
public class Despesa implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4573885127169522196L;

	@Id
	@GeneratedValue
	private Integer cod_despesa;
	
	@Column(name="descricao", length=100, nullable=false)
	private String descricao;
	
	@Column(columnDefinition="Decimal(10,2)")
	private float valor;	
	
	@Temporal(TemporalType.DATE)
	private Calendar data_cadastro;
	
	@Temporal(TemporalType.DATE)
	private Calendar data_atualizacao;
	
	@ManyToOne
	@JoinColumn(name="cod_familia")
	private Familia familia;

	public Integer getCod_despesa() {
		return cod_despesa;
	}

	public void setCod_despesa(Integer cod_despesa) {
		this.cod_despesa = cod_despesa;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public Calendar getData_cadastro() {
		return data_cadastro;
	}

	public void setData_cadastro(Calendar data_cadastro) {
		this.data_cadastro = data_cadastro;
	}

	public Calendar getData_atualizacao() {
		return data_atualizacao;
	}

	public void setData_atualizacao(Calendar data_atualizacao) {
		this.data_atualizacao = data_atualizacao;
	}

	public Familia getFamilia() {
		return familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cod_despesa == null) ? 0 : cod_despesa.hashCode());
		result = prime
				* result
				+ ((data_atualizacao == null) ? 0 : data_atualizacao.hashCode());
		result = prime * result
				+ ((data_cadastro == null) ? 0 : data_cadastro.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((familia == null) ? 0 : familia.hashCode());
		result = prime * result + Float.floatToIntBits(valor);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Despesa other = (Despesa) obj;
		if (cod_despesa == null) {
			if (other.cod_despesa != null)
				return false;
		} else if (!cod_despesa.equals(other.cod_despesa))
			return false;
		if (data_atualizacao == null) {
			if (other.data_atualizacao != null)
				return false;
		} else if (!data_atualizacao.equals(other.data_atualizacao))
			return false;
		if (data_cadastro == null) {
			if (other.data_cadastro != null)
				return false;
		} else if (!data_cadastro.equals(other.data_cadastro))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (familia == null) {
			if (other.familia != null)
				return false;
		} else if (!familia.equals(other.familia))
			return false;
		if (Float.floatToIntBits(valor) != Float.floatToIntBits(other.valor))
			return false;
		return true;
	}					
}