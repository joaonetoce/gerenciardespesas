package gerenciador.despesa;

import gerenciador.familia.Familia;

import java.util.List;

public interface DespesaDAO {

	public void salvar(Despesa despesa);
	public void atualizar(Despesa despesa);
	public void atualizarFamilia(Despesa despesa, Familia familia);
	public void excluir(Despesa despesa);
	public Despesa carregar(Integer codigo);
	public List<Despesa> listar(Familia familia);
}
