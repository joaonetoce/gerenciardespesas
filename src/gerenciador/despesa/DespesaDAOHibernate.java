package gerenciador.despesa;

import gerenciador.familia.Familia;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class DespesaDAOHibernate implements DespesaDAO {
	
	private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}

	@Override
	public void salvar(Despesa despesa) {
		this.session.save(despesa);
		
	}

	@Override
	public void atualizar(Despesa despesa) {
		String hql = "update Despesa set data_atualizacao = :data_atualizacao, descricao = :descricao, valor = :valor where cod_despesa = :cod_despesa";
		Query update = session.createQuery(hql);
		update.setParameter("cod_despesa", despesa.getCod_despesa());
		update.setParameter("descricao", despesa.getDescricao());
		update.setParameter("valor", despesa.getValor());
		update.setParameter("data_atualizacao", despesa.getData_atualizacao());		
		update.executeUpdate();		
	}
	
	@Override
	public void atualizarFamilia(Despesa despesa, Familia familia) {
		//String hql = "update Familia set ano = :ano, dia_do_ano = :dia_do_ano, data_prox_ajuste = :data_prox_ajuste, atualizacao_renda = :atualizacao_renda, dia_ajuste = :dia_ajuste, debito = :debito, saldo = :saldo where cod_familia = :cod_familia";
		String hql = "update Familia set atualizacao_renda = :atualizacao_renda, debito = :debito where cod_familia = :cod_familia";
		Query update = session.createQuery(hql);
		update.setParameter("cod_familia", familia.getCod_familia());
		update.setParameter("atualizacao_renda", familia.getAtualizacao_renda());
		update.setParameter("debito", familia.getDebito());		
		update.executeUpdate();		
	}

	@Override
	public void excluir(Despesa despesa) {
		this.session.delete(despesa);
		
	}

	@Override
	public Despesa carregar(Integer codigo) {
		return (Despesa) this.session.get(Despesa.class, codigo);
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Despesa> listar(Familia familia) {
		Criteria crit = session.createCriteria(Despesa.class);
		crit.add(Restrictions.eq("familia",familia));		
		return crit.list();
	}
}
