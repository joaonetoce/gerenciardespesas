package gerenciador.teste;

import gerenciador.familia.Familia;

public interface TesteDAO {

	public void atualizarDataAjusteFamilia(Familia familia);
	
}
