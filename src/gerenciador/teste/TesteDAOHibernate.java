package gerenciador.teste;

import gerenciador.familia.Familia;

import org.hibernate.Query;
import org.hibernate.Session;

public class TesteDAOHibernate implements TesteDAO {

	private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}

	@Override
	public void atualizarDataAjusteFamilia(Familia familia) {
		String hql = "update Familia set data_prox_ajuste = :data_prox_ajuste, atualizacao_renda = :atualizacao_renda where cod_familia = :cod_familia";
		Query update = session.createQuery(hql);
		update.setParameter("cod_familia", familia.getCod_familia());
		update.setParameter("data_prox_ajuste", familia.getData_prox_ajuste());
		update.setParameter("atualizacao_renda", familia.getAtualizacao_renda());
		update.executeUpdate();
		
	}
	
	
	
}
