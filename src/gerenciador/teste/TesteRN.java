package gerenciador.teste;

import java.util.Calendar;

import gerenciador.familia.Familia;
import gerenciador.util.DAOFactory;


public class TesteRN {
	
	private TesteDAO testeDAO;

	public TesteRN() {
		this.testeDAO = DAOFactory.criarTesteDAO();
	}
	
	public void atualizar(Familia familia){	
		
		familia.setAtualizacao_renda(Calendar.getInstance());
		this.testeDAO.atualizarDataAjusteFamilia(familia);
	}
}
