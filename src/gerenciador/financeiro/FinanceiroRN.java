package gerenciador.financeiro;

import java.util.Calendar;
import java.util.List;

import gerenciador.despesavariavel.DespesaVariavel;
import gerenciador.despesavariavel.DespesaVariavelRN;
import gerenciador.familia.Familia;
import gerenciador.familia.FamiliaRN;
import gerenciador.usuario.Usuario;
import gerenciador.util.DAOFactory;
import gerenciador.util.MyLibrary;


public class FinanceiroRN {
	
	private FinanceiroDAO financeiroDAO;

	public FinanceiroRN() {
		this.financeiroDAO = DAOFactory.criarFinanceiroDAO();
	}
	
	public void atualizar(){	
		Familia familia = new Familia();		
		Calendar dataAtual = Calendar.getInstance();
		Integer diaDoAnoDataAtual = dataAtual.get(Calendar.DAY_OF_YEAR);
		Integer anoDaDataAtual = dataAtual.get(Calendar.YEAR);
		
		MyLibrary library = new MyLibrary();
		Usuario usuarioLogado = library.buscarUsuarioLogado();
		
		FamiliaRN familiaRN = new FamiliaRN();
		Familia dadosFamiliaAntigo = familiaRN.carregar(usuarioLogado.getFamilia().getCod_familia());
		Integer diaDoAnoDoProxAjuste = dadosFamiliaAntigo.getData_prox_ajuste().get(Calendar.DAY_OF_YEAR);
		Integer anoDoProxAjuste = dadosFamiliaAntigo.getData_prox_ajuste().get(Calendar.YEAR);
						
		if(diaDoAnoDataAtual >= diaDoAnoDoProxAjuste && anoDaDataAtual.equals(anoDoProxAjuste)){
			familia.setAtualizacao_renda(Calendar.getInstance());
			Calendar dataAjuste = library.obterDataDebito(dadosFamiliaAntigo.getData_prox_ajuste().get(Calendar.DAY_OF_MONTH));
			familia.setData_prox_ajuste(dataAjuste);
			
			float saldoAnterior = dadosFamiliaAntigo.getSaldo();
			float debitoTotal = dadosFamiliaAntigo.getDebito();
			float rendaTotal = dadosFamiliaAntigo.getRenda() + dadosFamiliaAntigo.getRenda_adicional();
			float saldoFinal = (saldoAnterior - debitoTotal) + rendaTotal;
			
			familia.setSaldo(saldoFinal);
			familia.setRenda(dadosFamiliaAntigo.getRenda());
			familia.setCod_familia(dadosFamiliaAntigo.getCod_familia());
			
			this.financeiroDAO.atualizarFinancasFamilia(familia);
			this.atualizarDespesaVariavel();
		}
	}
	
	public void atualizarDespesaVariavel(){
		DespesaVariavelRN despesaVariavelRN = new DespesaVariavelRN();
		
		List<DespesaVariavel> despesasVariaveis = despesaVariavelRN.listarPorFamilia();		
		float valorTotal = 0;
		for(DespesaVariavel despesaVariavel : despesasVariaveis){
			valorTotal = valorTotal + despesaVariavel.getValor();			
		}
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>> VALOR TOTAL CALCULADO <<<<<<<<<<<<<<<<<<<<<<<");
		System.out.println("VALOR TOTAL: " + valorTotal);
		
		for(DespesaVariavel despesaVariavel : despesasVariaveis){
			DespesaVariavel despesaVariavelCarregado = new DespesaVariavel();
			despesaVariavelCarregado = despesaVariavelRN.carregar(despesaVariavel.getCod_despesavariavel());
			despesaVariavelRN.excluir(despesaVariavelCarregado);
			System.out.println("DESPESA VARIÁVEL EXCLUIDA: " + despesaVariavelCarregado.getDescricao());
		}
		
	}
}
