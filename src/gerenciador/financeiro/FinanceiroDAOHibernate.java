package gerenciador.financeiro;

import gerenciador.familia.Familia;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

public class FinanceiroDAOHibernate implements FinanceiroDAO {

	private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}
	
	@Override
	public void atualizarFinancasFamilia(Familia familia) {
		String hql = "update Familia set renda = :renda, data_prox_ajuste = :data_prox_ajuste, atualizacao_renda = :atualizacao_renda, saldo = :saldo where cod_familia = :cod_familia";
		Query update = session.createQuery(hql);
		update.setParameter("cod_familia", familia.getCod_familia());
		update.setParameter("data_prox_ajuste", familia.getData_prox_ajuste());
		update.setParameter("saldo", familia.getSaldo());
		update.setParameter("atualizacao_renda", familia.getAtualizacao_renda());
		update.setParameter("renda", familia.getRenda());
		update.executeUpdate();		
	}

	@Override
	public List<Familia> listarFinancaFamilia() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
