package gerenciador.financeiro;

import java.util.List;

import gerenciador.familia.Familia;

public interface FinanceiroDAO {

	public void atualizarFinancasFamilia(Familia familia);
	public List<Familia> listarFinancaFamilia();
}
