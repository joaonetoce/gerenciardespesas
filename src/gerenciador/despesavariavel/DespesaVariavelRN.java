package gerenciador.despesavariavel;

import java.util.Calendar;
import java.util.List;

import gerenciador.usuario.Usuario;
import gerenciador.util.DAOFactory;
import gerenciador.util.MyLibrary;

public class DespesaVariavelRN {

	private DespesaVariavelDAO despesaVariavelDAO;

	public DespesaVariavelRN() {
		this.despesaVariavelDAO = DAOFactory.criarDespesaVariavelDAO();
	}
	
	public DespesaVariavel carregar(Integer codigo){
		return this.despesaVariavelDAO.carregar(codigo);
	}
	
	public void salvar(DespesaVariavel despesaVariavel){
		despesaVariavel.setData_atualizacao(Calendar.getInstance());
		Integer codigo = despesaVariavel.getCod_despesavariavel();
		if(codigo == null || codigo == 0){						
			despesaVariavel.setData_cadastro(Calendar.getInstance());			
			this.despesaVariavelDAO.salvar(despesaVariavel);
		}else{			
			this.despesaVariavelDAO.atualizar(despesaVariavel);
		}
	}
	
	public List<DespesaVariavel> listarPorUsuario(){
		MyLibrary library = new MyLibrary();
		Usuario usuarioLogado = library.buscarUsuarioLogado();
		return this.despesaVariavelDAO.listarPorUsuario(usuarioLogado.getFamilia(), usuarioLogado);
	}
	
	public List<DespesaVariavel> listarPorFamilia(){
		MyLibrary library = new MyLibrary();
		Usuario usuarioLogado = library.buscarUsuarioLogado();
		return this.despesaVariavelDAO.listarPorFamilia(usuarioLogado.getFamilia());
	}
	
	public void excluir(DespesaVariavel despesaVariavel){
		this.despesaVariavelDAO.excluir(despesaVariavel);
	}
}
