package gerenciador.despesavariavel;

import gerenciador.familia.Familia;
import gerenciador.usuario.Usuario;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="despesavariavel")
public class DespesaVariavel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6710088144349412741L;

	@Id
	@GeneratedValue
	private Integer cod_despesavariavel;
	
	@Column(name="descricao", length=100, nullable=false)
	private String descricao;
	
	@Column(columnDefinition="Decimal(10,2)")
	private float valor;
	
	@Column(name="fonterenda", length=11)
	private Integer fonterenda;
	
	@Temporal(TemporalType.DATE)
	private Calendar data_cadastro;
	
	@Temporal(TemporalType.DATE)
	private Calendar data_atualizacao;
	
	@ManyToOne
	@JoinColumn(name="cod_familia")
	private Familia familia;
	
	@ManyToOne
	@JoinColumn(name="cod_usuario")
	private Usuario usuario;

	public Integer getCod_despesavariavel() {
		return cod_despesavariavel;
	}

	public void setCod_despesavariavel(Integer cod_despesavariavel) {
		this.cod_despesavariavel = cod_despesavariavel;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public Integer getFonterenda() {
		return fonterenda;
	}

	public void setFonterenda(Integer fonterenda) {
		this.fonterenda = fonterenda;
	}

	public Calendar getData_cadastro() {
		return data_cadastro;
	}

	public void setData_cadastro(Calendar data_cadastro) {
		this.data_cadastro = data_cadastro;
	}

	public Calendar getData_atualizacao() {
		return data_atualizacao;
	}

	public void setData_atualizacao(Calendar data_atualizacao) {
		this.data_atualizacao = data_atualizacao;
	}

	public Familia getFamilia() {
		return familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cod_despesavariavel == null) ? 0 : cod_despesavariavel
						.hashCode());
		result = prime
				* result
				+ ((data_atualizacao == null) ? 0 : data_atualizacao.hashCode());
		result = prime * result
				+ ((data_cadastro == null) ? 0 : data_cadastro.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((familia == null) ? 0 : familia.hashCode());
		result = prime * result
				+ ((fonterenda == null) ? 0 : fonterenda.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		result = prime * result + Float.floatToIntBits(valor);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DespesaVariavel other = (DespesaVariavel) obj;
		if (cod_despesavariavel == null) {
			if (other.cod_despesavariavel != null)
				return false;
		} else if (!cod_despesavariavel.equals(other.cod_despesavariavel))
			return false;
		if (data_atualizacao == null) {
			if (other.data_atualizacao != null)
				return false;
		} else if (!data_atualizacao.equals(other.data_atualizacao))
			return false;
		if (data_cadastro == null) {
			if (other.data_cadastro != null)
				return false;
		} else if (!data_cadastro.equals(other.data_cadastro))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (familia == null) {
			if (other.familia != null)
				return false;
		} else if (!familia.equals(other.familia))
			return false;
		if (fonterenda == null) {
			if (other.fonterenda != null)
				return false;
		} else if (!fonterenda.equals(other.fonterenda))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		if (Float.floatToIntBits(valor) != Float.floatToIntBits(other.valor))
			return false;
		return true;
	}
}