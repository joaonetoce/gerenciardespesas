package gerenciador.despesavariavel;

import gerenciador.familia.Familia;
import gerenciador.usuario.Usuario;

import java.util.List;

public interface DespesaVariavelDAO {

	public void salvar(DespesaVariavel despesaVariavel);
	public void atualizar(DespesaVariavel despesaVariavel);
	public void excluir(DespesaVariavel despesaVariavel);
	public DespesaVariavel carregar(Integer codigo);
	public List<DespesaVariavel> listarPorUsuario(Familia familia, Usuario usuario);
	public List<DespesaVariavel> listarPorFamilia(Familia familia);
}
