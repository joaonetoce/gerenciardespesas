package gerenciador.despesavariavel;

import gerenciador.familia.Familia;
import gerenciador.usuario.Usuario;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class DespesaVariavelDAOHibernate implements DespesaVariavelDAO {

	private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}
	
	@Override
	public void salvar(DespesaVariavel despesaVariavel) {
		this.session.save(despesaVariavel);		
	}

	@Override
	public void atualizar(DespesaVariavel despesaVariavel) {
		String hql = "update DespesaVariavel set valor = :valor, fonterenda = :fonterenda, data_atualizacao = :data_atualizacao, descricao = :descricao where cod_despesaVariavel = :cod_despesaVariavel";
		Query update = session.createQuery(hql);
		update.setParameter("cod_despesaVariavel", despesaVariavel.getCod_despesavariavel());
		update.setParameter("data_atualizacao", despesaVariavel.getData_atualizacao());
		update.setParameter("descricao", despesaVariavel.getDescricao());
		update.setParameter("fonterenda", despesaVariavel.getFonterenda());
		update.setParameter("valor", despesaVariavel.getValor());
		update.executeUpdate();			
	}

	@Override
	public void excluir(DespesaVariavel despesaVariavel) {
		this.session.delete(despesaVariavel);
	}

	@Override
	public DespesaVariavel carregar(Integer codigo) {
		return (DespesaVariavel) this.session.get(DespesaVariavel.class, codigo);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DespesaVariavel> listarPorUsuario(Familia familia, Usuario usuario) {
		Criteria crit = session.createCriteria(DespesaVariavel.class);
		crit.add(Restrictions.eq("familia",familia));
		crit.add(Restrictions.eq("usuario",usuario));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DespesaVariavel> listarPorFamilia(Familia familia) {
		Criteria crit = session.createCriteria(DespesaVariavel.class);
		crit.add(Restrictions.eq("familia",familia));
		return crit.list();
	}

}
