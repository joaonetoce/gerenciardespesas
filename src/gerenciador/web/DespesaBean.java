package gerenciador.web;

import java.util.Calendar;
import java.util.List;

import gerenciador.despesa.Despesa;
import gerenciador.despesa.DespesaRN;
import gerenciador.util.MyLibrary;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="despesaBean")
@RequestScoped
public class DespesaBean {

	private Despesa despesa = new Despesa();
	private List<Despesa> listaDespesas;
	private Integer cod_familia;
	private Calendar compData;
	private String destinoSalvar;
	
	public String salvar(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			DespesaRN despesaRN = new DespesaRN();
			despesaRN.salvar(this.despesa);		
			return "despesa-refresh";
		} catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível salvar essa despesa.");
			context.addMessage(null, facesMessage);	
			return null;
		}
	}
	
	public String editar(){
		return "despesa-form";
	}
	
	public String excluir(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			DespesaRN despesaRN = new DespesaRN();
			despesaRN.excluir(this.despesa);
			this.listaDespesas = null;
			return null;
		} catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível excluir essa despesa.");
			context.addMessage(null, facesMessage);	
			return null;
		}
	}
	
	public String carregarDebitos(){
		return "despesa";
	}

	public Despesa getDespesa() {
		return despesa;
	}

	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}

	public Integer getCod_familia() {
		return cod_familia;
	}

	public void setCod_familia(Integer cod_familia) {
		this.cod_familia = cod_familia;
	}

	public List<Despesa> getListaDespesas() {
		if(this.listaDespesas == null){
			DespesaRN despesaRN = new DespesaRN();
			this.listaDespesas = despesaRN.listar();
		}
		return this.listaDespesas;
	}

	public void setListaDespesas(List<Despesa> listaDespesas) {
		this.listaDespesas = listaDespesas;
	}

	public Calendar getCompData() {
		MyLibrary library = new MyLibrary();
		this.compData = library.complementoData();
		return this.compData;
	}

	public void setCompData(Calendar compData) {
		this.compData = compData;
	}

	public String getDestinoSalvar() {
		return destinoSalvar;
	}

	public void setDestinoSalvar(String destinoSalvar) {
		this.destinoSalvar = destinoSalvar;
	}	
}