package gerenciador.web;

import java.util.List;

import gerenciador.familia.Familia;
import gerenciador.familia.FamiliaRN;
import gerenciador.usuario.Usuario;
import gerenciador.util.MyLibrary;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="familiaBean")
@RequestScoped
public class FamiliaBean {
	
	private Familia familia = new Familia();
	private List<Familia> lista;
	private Familia dadosFamilia;
	private String legenda;
	private String quantRegistroFamilia;
				
	public String salvar(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			FamiliaRN familiaRN = new FamiliaRN();
			familiaRN.salvar(this.familia);
			return "familia";
		} catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível salvar essa família.");
			context.addMessage(null, facesMessage);	
			return null;
		}		
	}
	
	public String editar(){
		return "familia-form";
	}
	
	public String excluir(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			FamiliaRN familiaRN = new FamiliaRN();
			familiaRN.excluir(this.familia);
			this.lista = null;
			return null;
		} catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível excluir essa família.");
			context.addMessage(null, facesMessage);	
			return null;
		}
	}	
	
	public Familia getFamilia() {
		return familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;		
	}

	public List<Familia> getLista() {
		if(this.lista == null){
			FamiliaRN familiaRN = new FamiliaRN();
			this.lista = familiaRN.listar();			
		}		
		return this.lista;
	}

	public void setLista(List<Familia> lista) {
		this.lista = lista;
	}
	
	public String getLegenda() {		
		if(this.familia.getCod_familia()==null){			
			this.setLegenda("Nova família");
			return this.legenda;
		}else{
			this.setLegenda("Editar a família " + this.familia.getSobrenome());
			return this.legenda;			
		}		
	}

	public void setLegenda(String legenda) {
		this.legenda = legenda;
	}

	public Familia getDadosFamilia() {
		FamiliaRN familiaRN = new FamiliaRN();
		MyLibrary library = new MyLibrary();
		Usuario usuarioLogado = library.buscarUsuarioLogado();
		this.dadosFamilia = familiaRN.carregar(usuarioLogado.getFamilia().getCod_familia());
		return this.dadosFamilia;
	}

	public void setDadosFamilia(Familia dadosFamilia) {
		this.dadosFamilia = dadosFamilia;
	}

	public String getQuantRegistroFamilia() {
		FamiliaRN familiaRN = new FamiliaRN();
		Integer quantRegistros = familiaRN.listar().size();
		if(quantRegistros == 0){
			this.quantRegistroFamilia = "hidden";
			return this.quantRegistroFamilia;
		}else{
			this.quantRegistroFamilia = null;
			return this.quantRegistroFamilia;
		}		
	}

	public void setQuantRegistroFamilia(String quantRegistroFamilia) {
		this.quantRegistroFamilia = quantRegistroFamilia;
	}
}