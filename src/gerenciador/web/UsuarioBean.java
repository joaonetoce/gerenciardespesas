package gerenciador.web;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.springframework.security.core.context.SecurityContextHolder;

import gerenciador.familia.Familia;
import gerenciador.familia.FamiliaRN;
import gerenciador.usuario.Usuario;
import gerenciador.usuario.UsuarioRN;
import gerenciador.util.MyLibrary;

@ManagedBean(name="usuarioBean")
@RequestScoped
public class UsuarioBean {
	
	private Usuario usuario = new Usuario();
	private List<Usuario> listaUsuarios;
	private List<Usuario> listaAdministradores;
	private String legenda;
	private Integer cod_familia;
	private String formDestino;
	private String destinoSalvar;
	private String permissao;
	private String dadosUser;
	private String testeMetodo;
	
	private String selectFamilia;
	private String disabled;
	private String lblFamilia;
	
	public String novo(){
		return this.formDestino;
	}
	
	public String salvar(){	
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			UsuarioRN usuarioRN = new UsuarioRN();
			FamiliaRN familiaRN = new FamiliaRN();
			if(this.cod_familia == null){			
				this.usuario.setTipo(0);
			}else{
				Familia familia = familiaRN.carregar(this.cod_familia);
				this.usuario.setFamilia(familia);
				this.usuario.setTipo(1);			
			}
			usuarioRN.salvar(this.usuario);			
			return this.destinoSalvar;
		} catch (NoSuchAlgorithmException e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível salvar esse usuario.");
			context.addMessage(null, facesMessage);	
			return null;
		}
	}
	
	public String editar(){
		return this.formDestino;
	}
	
	public String editarMeuUsuario(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			UsuarioRN usuarioRN = new UsuarioRN();
			MyLibrary library = new MyLibrary();
			Usuario usuarioLogado = library.buscarUsuarioLogado();
			this.usuario = usuarioRN.carregar(usuarioLogado.getCod_usuario());
			return this.formDestino;
		} catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível editar esse usuario.");
			context.addMessage(null, facesMessage);	
			return null;
		}
	}
	
	public String alterarSenha(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			String senha = this.usuario.getSenha();
			if(!senha.equals(this.usuario.getConfirma_senha())){
				FacesMessage facesMessage = new FacesMessage("A senha não foi confirmada corretamente");
				context.addMessage(null, facesMessage);
				return null;
			}
			UsuarioRN usuarioRN = new UsuarioRN();
			MyLibrary library = new MyLibrary();
			Usuario usuarioLogado = library.buscarUsuarioLogado();
			this.usuario.setCod_usuario(usuarioLogado.getCod_usuario());
			usuarioRN.alterarSenha(this.usuario);
			FacesMessage facesMessage = new FacesMessage("Senha alterada com sucesso!");
			context.addMessage(null, facesMessage);
			return null;
		} catch (NoSuchAlgorithmException e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível alterar a senha.");
			context.addMessage(null, facesMessage);	
			return null;
		}
	}
	
	public String excluirUsuario(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			UsuarioRN usuarioRN = new UsuarioRN();
			usuarioRN.excluir(this.usuario);
			this.listaUsuarios = null;
			return null;
		} catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível excluir esse usuário.");
			context.addMessage(null, facesMessage);	
			return null;
		}
	}
	
	public String excluirAdministrador(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			UsuarioRN usuarioRN = new UsuarioRN();
			usuarioRN.excluir(this.usuario);
			this.listaAdministradores = null;
			return null;
		} catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível excluir esse administrador.");
			context.addMessage(null, facesMessage);	
			return null;
		}
	}
	
	public String teste(Integer codigo){
		
		this.testeMetodo = codigo.toString();
		return this.testeMetodo;
	}
	
	public String ativar(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			if(this.usuario.isAtivo())
				this.usuario.setAtivo(false);
			else
				this.usuario.setAtivo(true);
			
			UsuarioRN usuarioRN = new UsuarioRN();
			usuarioRN.salvar(this.usuario);
			return null;
		} catch (NoSuchAlgorithmException e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível alterar esse usuário.");
			context.addMessage(null, facesMessage);	
			return null;
		}
	}
	
	public String atribuiPermissao(Usuario usuario, String permissao) {

		this.usuario = usuario;

		Set<String> permissoes = this.usuario.getPermissao();

		if (permissoes.contains(permissao)) {
			permissoes.remove(permissao);
		} else {
			permissoes.add(permissao);
		}
		return null;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getLegenda() {
		if(this.usuario.getCod_usuario()==null){			
			this.setLegenda("Novo usuário");
			return this.legenda;
		}else{
			this.setLegenda("Editar - " + this.usuario.getNome());
			return this.legenda;			
		}
	}
	public void setLegenda(String legenda) {
		this.legenda = legenda;
	}

	public Integer getCod_familia() {
				
		if(this.usuario.getCod_usuario() != null && this.usuario.getFamilia() != null){			
			UsuarioRN usuarioRN = new UsuarioRN();
			Usuario user = usuarioRN.carregar(this.usuario.getCod_usuario());
			this.cod_familia = user.getFamilia().getCod_familia();
		}
		return this.cod_familia;
	}

	public void setCod_familia(Integer cod_familia) {
		this.cod_familia = cod_familia;
	}

	public String getPermissao() {
		return permissao;
	}

	public void setPermissao(String permissao) {
		this.permissao = permissao;
	}

	public String getSelectFamilia() {
		this.selectFamilia = "Selecione";
		if(this.usuario.getFamilia() == null && this.usuario.getCod_usuario() != null){
			this.selectFamilia = "Usuário Administrador";
		}
		return this.selectFamilia;
	}

	public void setSelectFamilia(String selectFamilia) {
		this.selectFamilia = selectFamilia;
	}

	public String getDisabled() {
		if(this.usuario.getFamilia() == null && this.usuario.getCod_usuario() != null){
			this.disabled = "true";
		}
		return this.disabled;
	}

	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}

	public String getLblFamilia() {
		this.lblFamilia = "Família";
		if(this.usuario.getFamilia() == null && this.usuario.getCod_usuario() != null){
			this.lblFamilia = "*";
		}
		return this.lblFamilia;
	}

	public void setLblFamilia(String lblFamilia) {
		this.lblFamilia = lblFamilia;
	}

	public String getFormDestino() {
		return formDestino;
	}

	public void setFormDestino(String formDestino) {
		this.formDestino = formDestino;
	}

	public String getDadosUser() {
		UsuarioRN usuarioRN = new UsuarioRN();
		String loginUserLog = SecurityContextHolder.getContext().getAuthentication().getName();
		Usuario usuarioLogado = usuarioRN.buscarPorLogin(loginUserLog);
		if(usuarioLogado.getTipo() == 0){
			this.dadosUser = usuarioLogado.getNome() + " (Administrador)";
		}else{
			this.dadosUser = usuarioLogado.getNome();
		}
		return this.dadosUser;
	}

	public void setDadosUser(String dadosUser) {
		this.dadosUser = dadosUser;
	}

	public List<Usuario> getListaUsuarios() {
		if(this.listaUsuarios == null){
			UsuarioRN usuarioRN = new UsuarioRN();
			this.listaUsuarios = usuarioRN.listarUsuarios();			
		}		
		return this.listaUsuarios;
	}

	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<Usuario> getListaAdministradores() {
		if(this.listaAdministradores == null){
			UsuarioRN usuarioRN = new UsuarioRN();
			this.listaAdministradores = usuarioRN.listarAdministradores();			
		}		
		return this.listaAdministradores;
	}

	public void setListaAdministradores(List<Usuario> listaAdministradores) {
		this.listaAdministradores = listaAdministradores;
	}

	public String getDestinoSalvar() {
		return destinoSalvar;
	}

	public void setDestinoSalvar(String destinoSalvar) {
		this.destinoSalvar = destinoSalvar;
	}	
}