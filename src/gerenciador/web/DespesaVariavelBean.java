package gerenciador.web;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import gerenciador.despesavariavel.DespesaVariavel;
import gerenciador.despesavariavel.DespesaVariavelRN;
import gerenciador.familia.Familia;
import gerenciador.familia.FamiliaRN;
import gerenciador.usuario.Usuario;
import gerenciador.usuario.UsuarioRN;
import gerenciador.util.MyLibrary;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="despesaVariavelBean")
@RequestScoped
public class DespesaVariavelBean {

	private DespesaVariavel despesaVariavel = new DespesaVariavel();
	private List<DespesaVariavel> listarDespesasVariavel;
	private List<DespesaVariavel> listarDespesasVariavelPorUsuario;
	private String destinoSalvar;
	private String disabled;
	
	public String salvar() throws NoSuchAlgorithmException {
		FacesContext context = FacesContext.getCurrentInstance();		
		try {
			MyLibrary library = new MyLibrary();
			Usuario usuarioLogado = library.buscarUsuarioLogado();
			
			FamiliaRN familiaRN = new FamiliaRN();
			Familia familia = familiaRN.carregar(usuarioLogado.getFamilia().getCod_familia());
			
			UsuarioRN usuarioRN = new UsuarioRN();
			Usuario usuario = usuarioRN.carregar(usuarioLogado.getCod_usuario());
					
			DespesaVariavelRN despesaVariavelRN = new DespesaVariavelRN();
			this.despesaVariavel.setUsuario(usuarioLogado);
			this.despesaVariavel.setFamilia(usuarioLogado.getFamilia());
			
			float valorDespesaAnterior = 0;
			if(this.despesaVariavel.getCod_despesavariavel() != null){
				DespesaVariavel despesaVariavelAntiga = despesaVariavelRN.carregar(this.despesaVariavel.getCod_despesavariavel());
				valorDespesaAnterior = despesaVariavelAntiga.getValor();
				this.despesaVariavel.setFonterenda(despesaVariavelAntiga.getFonterenda());
			}else{
				if(this.despesaVariavel.getFonterenda() == null && this.despesaVariavel.getCod_despesavariavel() == null){
					FacesMessage facesMessage = new FacesMessage("É preciso selecionar uma fonte de renda.");
					context.addMessage(null, facesMessage);	
					return null;
				}				
			}
			float saldoFamilia = familia.getSaldo();
			float valorDespesaVariavel = this.despesaVariavel.getValor();
			float debitoFamilia = familia.getDebito();
			float reservaFamilia = familia.getReserva();
			float orcamentoUsuario = usuario.getOrcamento();
			float saldoFinalFamilia = 0;
			float debitoFinalFamilia = 0;
			float reservaFinalFamilia = 0;
			float orcamentoFinalUsuario = 0;
						
			if(this.despesaVariavel.getFonterenda().equals(0)){
				float saldoAtualFamilia = saldoFamilia + valorDespesaAnterior;
				if(this.despesaVariavel.getValor() > saldoAtualFamilia){
					FacesMessage facesMessage = new FacesMessage("O saldo familiar disponível é de R$" + saldoAtualFamilia);
					context.addMessage(null, facesMessage);	
					return null;
				}
				else{					
					saldoFinalFamilia = saldoAtualFamilia - valorDespesaVariavel;
					familia.setSaldo(saldoFinalFamilia);
					despesaVariavelRN.salvar(this.despesaVariavel);
					familiaRN.salvar(familia);
					return "despesavariavel-refresh";
				}
			}if(this.despesaVariavel.getFonterenda().equals(1)){
				debitoFinalFamilia = (debitoFamilia - valorDespesaAnterior) + valorDespesaVariavel;
				familia.setDebito(debitoFinalFamilia);
				despesaVariavelRN.salvar(this.despesaVariavel);
				familiaRN.salvar(familia);
				return "despesavariavel-refresh";
			}if(this.despesaVariavel.getFonterenda().equals(2)){
				float reservaAtualFamilia = reservaFamilia + valorDespesaAnterior;
				if(this.despesaVariavel.getValor() > reservaAtualFamilia){
					FacesMessage facesMessage = new FacesMessage("A reserva econômica familiar disponível é de R$" + reservaAtualFamilia);
					context.addMessage(null, facesMessage);	
					return null;
				}else{
					reservaFinalFamilia = reservaAtualFamilia - valorDespesaVariavel;
					familia.setReserva(reservaFinalFamilia);
					despesaVariavelRN.salvar(this.despesaVariavel);
					familiaRN.salvar(familia);					
					return "despesavariavel-refresh";
				}
			}else{
				orcamentoFinalUsuario = (orcamentoUsuario - valorDespesaAnterior) + valorDespesaVariavel;
				usuario.setOrcamento(orcamentoFinalUsuario);
				despesaVariavelRN.salvar(this.despesaVariavel);
				usuarioRN.salvar(usuario);
				return "despesavariavel-refresh";
			}
			
		}
		catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível salvar essa despesa.");
			context.addMessage(null, facesMessage);	
			return null;
		}finally{
			
		}		
	}
		
	public String editar(){		
		return "despesavariavel-form";
	}
	
	public String excluir() throws NoSuchAlgorithmException{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			FamiliaRN familiaRN = new FamiliaRN();
			Familia familia = familiaRN.carregar(this.despesaVariavel.getFamilia().getCod_familia());
			
			DespesaVariavelRN despesaVariavelRN = new DespesaVariavelRN();
			
			if(this.despesaVariavel.getFonterenda().equals(0)){
				float saldoFinalFamilia = familia.getSaldo() + this.despesaVariavel.getValor();
				familia.setSaldo(saldoFinalFamilia);
				familiaRN.salvar(familia);
				despesaVariavelRN.excluir(this.despesaVariavel);
				this.listarDespesasVariavel = null;
				return "despesavariavel-refresh";
			}
			if(this.despesaVariavel.getFonterenda().equals(1)){
				float debitoFinalFamiliar = familia.getDebito() - this.despesaVariavel.getValor();
				familia.setDebito(debitoFinalFamiliar);
				familiaRN.salvar(familia);
				despesaVariavelRN.excluir(this.despesaVariavel);
				this.listarDespesasVariavel = null;
				return "despesavariavel-refresh";
			}
			if(this.despesaVariavel.getFonterenda().equals(2)){
				float reservaFinalFamiliar = familia.getReserva() + this.despesaVariavel.getValor();
				familia.setReserva(reservaFinalFamiliar);
				familiaRN.salvar(familia);
				despesaVariavelRN.excluir(this.despesaVariavel);
				this.listarDespesasVariavel = null;
				return "despesavariavel-refresh";
			}
			else{
				MyLibrary library = new MyLibrary();
				Usuario usuarioLogado = library.buscarUsuarioLogado();
				UsuarioRN usuarioRN = new UsuarioRN();
				Usuario usuario = usuarioRN.carregar(usuarioLogado.getCod_usuario());
				
				float orcamentoFinalUsuario = usuario.getOrcamento() - this.despesaVariavel.getValor();
				usuario.setOrcamento(orcamentoFinalUsuario);
				usuarioRN.salvar(usuario);
				despesaVariavelRN.excluir(this.despesaVariavel);
				this.listarDespesasVariavel = null;
				return "despesavariavel-refresh";
			}
		} catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível excluir essa despesa.");
			context.addMessage(null, facesMessage);	
			return null;
		}			
	}
	
	public DespesaVariavel getDespesaVariavel() {
		return despesaVariavel;
	}
	public void setDespesaVariavel(DespesaVariavel despesaVariavel) {
		this.despesaVariavel = despesaVariavel;
	}
	public List<DespesaVariavel> getListarDespesasVariavel() {
		if(this.listarDespesasVariavel == null){
			DespesaVariavelRN despesaVariavelRN = new DespesaVariavelRN();
			this.listarDespesasVariavel = despesaVariavelRN.listarPorFamilia();
		}
		return this.listarDespesasVariavel;
	}
	public void setListarDespesasVariavel(
			List<DespesaVariavel> listarDespesasVariavel) {
		this.listarDespesasVariavel = listarDespesasVariavel;
	}
	public String getDestinoSalvar() {
		return destinoSalvar;
	}
	public void setDestinoSalvar(String destinoSalvar) {
		this.destinoSalvar = destinoSalvar;
	}

	public List<DespesaVariavel> getListarDespesasVariavelPorUsuario() {
		if(this.listarDespesasVariavelPorUsuario == null){
			DespesaVariavelRN despesaVariavelRN = new DespesaVariavelRN();
			this.listarDespesasVariavelPorUsuario = despesaVariavelRN.listarPorUsuario();			
		}
		return this.listarDespesasVariavelPorUsuario;
	}

	public void setListarDespesasVariavelPorUsuario(
			List<DespesaVariavel> listarDespesasVariavelPorUsuario) {
		this.listarDespesasVariavelPorUsuario = listarDespesasVariavelPorUsuario;
	}
	
	public String getDisabled() {
		if(this.despesaVariavel.getCod_despesavariavel() == null){
			this.disabled = "false";
			
		}else{			
			this.disabled = "true";
		}
		return this.disabled;
	}

	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}	
}