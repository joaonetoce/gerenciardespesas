package gerenciador.web;

import gerenciador.financeiro.FinanceiroRN;
import gerenciador.usuario.Usuario;
import gerenciador.usuario.UsuarioRN;
import gerenciador.util.MyLibrary;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="financeiroBean")
@RequestScoped
public class FinanceiroBean {
	private Usuario dadosUsuario;
	
	public void atualizacaoFinanceira(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			FinanceiroRN financeiroRN = new FinanceiroRN();			
			financeiroRN.atualizar();
		} catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível atualizar os dados financeiros.");
			context.addMessage(null, facesMessage);
		}	
	}

	public Usuario getDadosUsuario() {
		MyLibrary library = new MyLibrary();
		Usuario usuarioLogado = library.buscarUsuarioLogado();
		UsuarioRN usuarioRN = new UsuarioRN();
		this.dadosUsuario = usuarioRN.carregar(usuarioLogado.getCod_usuario());
		return this.dadosUsuario;
	}

	public void setDadosUsuario(Usuario dadosUsuario) {
		this.dadosUsuario = dadosUsuario;
	}
	
	
}
