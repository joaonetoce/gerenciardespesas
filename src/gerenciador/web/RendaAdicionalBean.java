package gerenciador.web;

import java.util.List;

import gerenciador.rendaadicional.RendaAdicional;
import gerenciador.rendaadicional.RendaAdicionalRN;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="rendaAdicionalBean")
@RequestScoped
public class RendaAdicionalBean {

	private RendaAdicional rendaAdicional = new RendaAdicional();
	private List<RendaAdicional> listaRendaAdicional;
	private String destinoSalvar;
	
	public String salvar(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			RendaAdicionalRN rendaAdicionalRN = new RendaAdicionalRN();
			rendaAdicionalRN.salvar(this.rendaAdicional);
			return "rendaadicional-refresh";
		} catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível salvar essa renda.");
			context.addMessage(null, facesMessage);	
			return null;
		}
	}
	
	public String editar(){
		return "rendaadicional-form";
	}
	
	public String excluir(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			RendaAdicionalRN rendaAdicionalRN = new RendaAdicionalRN();
			rendaAdicionalRN.excluir(this.rendaAdicional);
			this.listaRendaAdicional = null;
			return null;
		} catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível excluir essa renda.");
			context.addMessage(null, facesMessage);	
			return null;
		}
	}
	
	public RendaAdicional getRendaAdicional() {
		return rendaAdicional;
	}
	public void setRendaAdicional(RendaAdicional rendaAdicional) {
		this.rendaAdicional = rendaAdicional;
	}
	public List<RendaAdicional> getListaRendaAdicional() {
		if(this.listaRendaAdicional == null){
			RendaAdicionalRN rendaAdicionalRN = new RendaAdicionalRN();
			this.listaRendaAdicional = rendaAdicionalRN.listar();
		}
		return this.listaRendaAdicional;
	}
	public void setListaRendaAdicional(List<RendaAdicional> listaRendaAdicional) {
		this.listaRendaAdicional = listaRendaAdicional;
	}

	public String getDestinoSalvar() {
		return destinoSalvar;
	}

	public void setDestinoSalvar(String destinoSalvar) {
		this.destinoSalvar = destinoSalvar;
	}	
}
