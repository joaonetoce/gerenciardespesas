package gerenciador.web;

import java.util.Calendar;

import gerenciador.familia.Familia;
import gerenciador.teste.TesteRN;
import gerenciador.util.MyLibrary;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="testeBean")
@RequestScoped
public class testeBean {

	private Familia familia = new Familia();
	private Integer cod_familia;
	private Integer dia;
	
	public String salvar(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			TesteRN testeRN = new TesteRN();
			this.familia.setCod_familia(this.cod_familia);
			
			MyLibrary library = new MyLibrary();
			Calendar dataAjuste = library.testeInserirData(this.dia);
			
			this.familia.setData_prox_ajuste(dataAjuste);
			testeRN.atualizar(this.familia);
			return "home";
		} catch (Exception e) {
			FacesMessage facesMessage = new FacesMessage("Desculpa! Não foi possível salvar essa alteração.");
			context.addMessage(null, facesMessage);	
			return null;
		}
	}

	public Familia getFamilia() {
		return familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;
	}

	public Integer getCod_familia() {
		return cod_familia;
	}

	public void setCod_familia(Integer cod_familia) {
		this.cod_familia = cod_familia;
	}

	public Integer getDia() {
		return dia;
	}

	public void setDia(Integer dia) {
		this.dia = dia;
	}	
}
