package gerenciador.familia;

import gerenciador.usuario.Usuario;
import gerenciador.usuario.UsuarioRN;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class FamiliaDAOHibernate implements FamiliaDAO {

	private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}
	
	@Override
	public void salvar(Familia familia) {
		this.session.save(familia);
	}
	
	@Override	
	public void atualizar(Familia familia) {
		String hql = "update Familia set reserva = :reserva, saldo = :saldo, telefone = :telefone, responsavel = :responsavel, atualizacao_renda = :atualizacao_renda, renda = :renda, sobrenome = :sobrenome where cod_familia = :cod_familia";
		Query update = session.createQuery(hql);
		update.setParameter("cod_familia", familia.getCod_familia());
		update.setParameter("atualizacao_renda", familia.getAtualizacao_renda());
		update.setParameter("renda", familia.getRenda());
		update.setParameter("sobrenome", familia.getSobrenome());		
		update.setParameter("responsavel", familia.getResponsavel());
		update.setParameter("telefone", familia.getTelefone());
		update.setParameter("saldo", familia.getSaldo());
		update.setParameter("reserva", familia.getReserva());
		update.executeUpdate();
	}

	@Override
	public void excluir(Familia familia) {
		this.session.delete(familia);
	}

	@Override
	public Familia carregar(Integer codigo) {
		return (Familia) this.session.get(Familia.class, codigo);
	}	

	@SuppressWarnings("unchecked")
	@Override
	public List<Familia> listar() {
		return this.session.createCriteria(Familia.class).list();
	}

	@Override
	public void excluirDespesa(Integer codigo) {
		String hql = "delete from Despesa where cod_familia = :codigo";
        Query delete = session.createQuery(hql);
        delete.setParameter("codigo", codigo);
        delete.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void excluirUsuario(Familia familia) {
		List<Usuario> usuarios = this.session.createCriteria(Usuario.class).list();
		for(Usuario usuario : usuarios){
			if(familia.equals(usuario.getFamilia())){
				Usuario usuarioCarregado = new Usuario();
				UsuarioRN usuarioRN = new UsuarioRN();
				usuarioCarregado = usuarioRN.carregar(usuario.getCod_usuario());				
				this.session.delete(usuarioCarregado);
			}
		}		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> listarUsuarioPorFamilia(Integer codigo_familia){
		Criteria crit = session.createCriteria(Usuario.class);
		crit.add(Restrictions.ne("cod_familia",codigo_familia));
		return crit.list();
	}

	@Override
	public void excluirRendaAdicional(Integer codigo) {
		String hql = "delete from RendaAdicional where cod_familia = :codigo";
		Query delete = session.createQuery(hql);
		delete.setParameter("codigo", codigo);
		delete.executeUpdate();	
	}

	@Override
	public void excluirDespesaVariavel(Integer codigo) {
		String hql = "delete from DespesaVariavel where cod_familia = :codigo";
		Query delete = session.createQuery(hql);
		delete.setParameter("codigo", codigo);
		delete.executeUpdate();
	}
}