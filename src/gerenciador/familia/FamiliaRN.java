package gerenciador.familia;

import java.util.Calendar;
import java.util.List;

import gerenciador.usuario.Usuario;
import gerenciador.usuario.UsuarioRN;
import gerenciador.util.DAOFactory;
import gerenciador.util.MyLibrary;

public class FamiliaRN {

	private FamiliaDAO familiaDAO;

	public FamiliaRN() {
		this.familiaDAO = DAOFactory.criarFamiliaDAO();
	}
	
	public Familia carregar(Integer codigo){
		return this.familiaDAO.carregar(codigo);
	}
	
	public void salvar(Familia familia){
		Integer codigo = familia.getCod_familia();
		familia.setAtualizacao_renda(Calendar.getInstance());
			
		if(codigo == null || codigo == 0){
			familia.setData_cadastro(Calendar.getInstance());		
			
			MyLibrary library = new MyLibrary();
			familia.setSaldo(0);
			familia.setReserva(0);
			Calendar dataAjuste = library.obterDataDebito(1);
			familia.setData_prox_ajuste(dataAjuste);
			familia.setDebito(0);
			familia.setSaldo(0);
			familia.setReserva(0);
			
			this.familiaDAO.salvar(familia);			
		}else{
			if(familia.getDebito() < 0){
				familia.setDebito(0);
			}
			this.familiaDAO.atualizar(familia);
		}
	}
	
	public void excluir(Familia familia){
		this.familiaDAO.excluirDespesaVariavel(familia.getCod_familia());
		this.familiaDAO.excluirDespesa(familia.getCod_familia());
		this.familiaDAO.excluirRendaAdicional(familia.getCod_familia());
				
			UsuarioRN usuarioRN = new UsuarioRN();
			List<Usuario> usuarios = usuarioRN.listarUsuarios();
			for(Usuario usuario : usuarios){
				if(familia.equals(usuario.getFamilia())){
					Usuario usuarioCarregado = new Usuario();
					usuarioCarregado = usuarioRN.carregar(usuario.getCod_usuario());				
					usuarioRN.excluir(usuarioCarregado);
				}
			}	
		
		this.familiaDAO.excluir(familia);
	}
	
	public List<Familia> listar(){
		return this.familiaDAO.listar();
	}
	
	public List<Usuario> listarUsuarioPorFamilia(Integer codigo){
		return this.familiaDAO.listarUsuarioPorFamilia(codigo);
	}
}