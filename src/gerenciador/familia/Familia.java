package gerenciador.familia;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="familia")
public class Familia implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -720094881295056668L;

	@Id
	@GeneratedValue
	private Integer cod_familia;
	
	@Column(name="sobrenome", length=100, nullable=false)
	private String sobrenome;
	
	@Column(name="renda", columnDefinition="Decimal(10,2)", nullable=true)
	private float renda;
	
	@Column(name="debito", columnDefinition="Decimal(10,2)", nullable=false)
	private float debito;
	
	@Column(name="renda_adicional", columnDefinition="Decimal(10,2)", nullable=true)
	private float renda_adicional;
	
	@Column(name="saldo", columnDefinition="Decimal(10,2)", nullable=false)
	private float saldo;
	
	@Column(name="reserva", columnDefinition="Decimal(10,2)")
	private float reserva;	
		
	@Column(name="responsavel", length=100, unique=true)
	private String responsavel;
	
	@Column(name="telefone", length=100, unique=true)
	private String telefone;
	
	@Temporal(TemporalType.DATE)
	private Calendar data_cadastro;
	
	@Temporal(TemporalType.DATE)
	private Calendar atualizacao_renda;
	
	@Temporal(TemporalType.DATE)
	private Calendar data_prox_ajuste;

	public Integer getCod_familia() {
		return cod_familia;
	}

	public void setCod_familia(Integer cod_familia) {
		this.cod_familia = cod_familia;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public float getRenda() {
		return renda;
	}

	public void setRenda(float renda) {
		this.renda = renda;
	}

	public float getDebito() {
		return debito;
	}

	public void setDebito(float debito) {
		this.debito = debito;
	}

	public float getRenda_adicional() {
		return renda_adicional;
	}

	public void setRenda_adicional(float renda_adicional) {
		this.renda_adicional = renda_adicional;
	}

	public float getSaldo() {
		return saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	public float getReserva() {
		return reserva;
	}

	public void setReserva(float reserva) {
		this.reserva = reserva;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Calendar getData_cadastro() {
		return data_cadastro;
	}

	public void setData_cadastro(Calendar data_cadastro) {
		this.data_cadastro = data_cadastro;
	}

	public Calendar getAtualizacao_renda() {
		return atualizacao_renda;
	}

	public void setAtualizacao_renda(Calendar atualizacao_renda) {
		this.atualizacao_renda = atualizacao_renda;
	}

	public Calendar getData_prox_ajuste() {
		return data_prox_ajuste;
	}

	public void setData_prox_ajuste(Calendar data_prox_ajuste) {
		this.data_prox_ajuste = data_prox_ajuste;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((atualizacao_renda == null) ? 0 : atualizacao_renda
						.hashCode());
		result = prime * result
				+ ((cod_familia == null) ? 0 : cod_familia.hashCode());
		result = prime * result
				+ ((data_cadastro == null) ? 0 : data_cadastro.hashCode());
		result = prime
				* result
				+ ((data_prox_ajuste == null) ? 0 : data_prox_ajuste.hashCode());
		result = prime * result + Float.floatToIntBits(debito);
		result = prime * result + Float.floatToIntBits(renda);
		result = prime * result + Float.floatToIntBits(renda_adicional);
		result = prime * result + Float.floatToIntBits(reserva);
		result = prime * result
				+ ((responsavel == null) ? 0 : responsavel.hashCode());
		result = prime * result + Float.floatToIntBits(saldo);
		result = prime * result
				+ ((sobrenome == null) ? 0 : sobrenome.hashCode());
		result = prime * result
				+ ((telefone == null) ? 0 : telefone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Familia other = (Familia) obj;
		if (atualizacao_renda == null) {
			if (other.atualizacao_renda != null)
				return false;
		} else if (!atualizacao_renda.equals(other.atualizacao_renda))
			return false;
		if (cod_familia == null) {
			if (other.cod_familia != null)
				return false;
		} else if (!cod_familia.equals(other.cod_familia))
			return false;
		if (data_cadastro == null) {
			if (other.data_cadastro != null)
				return false;
		} else if (!data_cadastro.equals(other.data_cadastro))
			return false;
		if (data_prox_ajuste == null) {
			if (other.data_prox_ajuste != null)
				return false;
		} else if (!data_prox_ajuste.equals(other.data_prox_ajuste))
			return false;
		if (Float.floatToIntBits(debito) != Float.floatToIntBits(other.debito))
			return false;
		if (Float.floatToIntBits(renda) != Float.floatToIntBits(other.renda))
			return false;
		if (Float.floatToIntBits(renda_adicional) != Float
				.floatToIntBits(other.renda_adicional))
			return false;
		if (Float.floatToIntBits(reserva) != Float
				.floatToIntBits(other.reserva))
			return false;
		if (responsavel == null) {
			if (other.responsavel != null)
				return false;
		} else if (!responsavel.equals(other.responsavel))
			return false;
		if (Float.floatToIntBits(saldo) != Float.floatToIntBits(other.saldo))
			return false;
		if (sobrenome == null) {
			if (other.sobrenome != null)
				return false;
		} else if (!sobrenome.equals(other.sobrenome))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		return true;
	}				
}