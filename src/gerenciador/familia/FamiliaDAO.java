package gerenciador.familia;

import gerenciador.usuario.Usuario;

import java.util.List;

public interface FamiliaDAO {

	public void salvar(Familia familia);
	public void atualizar(Familia familia);
	public void excluir(Familia familia);
	public void excluirDespesaVariavel(Integer codigo);
	public void excluirDespesa(Integer codigo);
	public void excluirRendaAdicional(Integer codigo);
	public void excluirUsuario(Familia familia);
	public Familia carregar(Integer codigo);
	public List<Familia> listar();
	public List<Usuario> listarUsuarioPorFamilia(Integer codigo_familia);
}
