$(document).ready(function(){

	mostrarAlterPass();
	ocultaAlterPass();
	maskMoney();
	maskDate();
	maskTel();
	maskCPF();
	maskCep();
	limiteCarac();
});


function mostrarAlterPass(){
	
	$(".alter-pass").addClass("hidden");	
	$(".add-alter-pass").click(function(event){
		  event.preventDefault();
		  $(".alter-pass").removeClass("hidden");
		});	
}

function ocultaAlterPass(){
	$(".rem-alter-pass").click(function(event){
		  event.preventDefault();		  
		  $(".alter-pass").addClass("hidden");		  
		});
}

function maskMoney(){
	$("input.money").maskMoney({showSymbol:false, symbolStay:false, symbol:"R$ ", decimal:",", thousands:"."});
}

function maskDate(){
	$("input.date").mask("99/99/9999");
}

function maskTel(){
	$("input.tel").mask("(99) 99999-9999");
}

function maskCPF(){
	$("input.cpf").mask("999.999.999-99");
}

function maskCep(){
	$("input.cep").mask("99999-999");
}

function limiteCarac(){
	$("input.limiteCarac").bind("input keyup paste", function(){
		var maximo = 15;
		var disponivel = maximo - $(this).val().length;

		if(disponivel < 0){
			var texto = $(this).val().substr(0, maximo);
			$(this).val(texto);
			disponivel = 0;
		}

		$(".contagem").text(disponivel);
	});
}